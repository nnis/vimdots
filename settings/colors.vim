""      ______      __
""     / ____/___  / /___  __________
""    / /   / __ \/ / __ \/ ___/ ___/
""   / /___/ /_/ / / /_/ / /  (__  )
""   \____/\____/_/\____/_/  /____/

"True colors on (alacritty needs the second hack to work)
set termguicolors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" For kitty from kovidgoyal
" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
"let &t_ut=''

"Enables cursor line position tracking:
set cursorline

"Enables cursor column position tracking:
set cursorcolumn

"Setting the colorscheme
colorscheme PaperColor
"colorscheme base16-irblack
"

"Setting the background to dark or light depending on the time of day
execute 'set background=' . (strftime('%H') < 17 ? 'light' : 'dark')

"Setting the airline colorscheme (can be different from overall colorscheme)
"let g:airline_theme='serene'
" Set color of current line
":hi CursorLine cterm=NONE ctermbg=darkgrey ctermfg=white guibg=#f2f2f2
" Set color of Window separator lines
":hi StatusLineNC cterm=NONE ctermbg=white ctermfg=white guifg=#f2f2f2 guibg=NONE
":hi VertSplit    cterm=NONE ctermbg=white ctermfg=white guifg=#f2f2f2 guibg=NONE
":hi LineNr       cterm=NONE ctermbg=white ctermfg=white guifg=#cccccc guibg=NONE

"Since I used the gruvbox theme, the following makes gitgutter seem transparent
"highlight SignColumn guibg=#666666

"Making the glyphs color coded
augroup my-glyph-palette
  autocmd! *
  autocmd FileType fern call glyph_palette#apply()
  autocmd FileType nerdtree,startify call glyph_palette#apply()
augroup END

"Default theme tweaks
"For dark theme
"For light theme
":hi CursorLine cterm=NONE ctermbg=grey ctermfg=white guibg=#dddddd
":highlight LineNr              guifg=#51565b
":highlight CursorLineNr        guifg=#51565b cterm=bold gui=bold
":highlight Comment             guifg=#555555 cterm=italic gui=italic
":highlight Visual              guibg=#f48024 guifg=#111111 cterm=italic gui=italic
":highlight Identifier          guifg=#3b9acd
":highlight Keyword             guifg=#00ff00
":highlight Character           guifg=#22aa22
":highlight String              guifg=#c09000
":highlight SpecialCharacter    guifg=#ffaa00
":highlight Operator            guifg=#ffcc00
":highlight Constant            guifg=#0040ff
":highlight Statement           guifg=#00ff00
