""          __          ____                     
""     ____/ /__ _   __/  _/________  ____  _____
""    / __  / _ \ | / // // ___/ __ \/ __ \/ ___/
""   / /_/ /  __/ |/ // // /__/ /_/ / / / (__  ) 
""   \__,_/\___/|___/___/\___/\____/_/ /_/____/  
""                                               

"Corrections on devicons size and appearance, credit Vim-Reaper
let g:webdevicons_enable = 1
let g:webdevicons_enable_unite = 1
let g:webdevicons_enable_denite = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_vimfiler = 1
let g:WebDevIconsUnicodeDecorateFileNodes = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1
let g:webdevicons_enable_airline_statusline = 1
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1

"Enabling open close distinction on icons (nice)
let g:DevIconsEnableFoldersOpenClose = 1
let g:WebDevIconsUnicodeDecorateFolderNodesDefaultSymbol = ''
let g:DevIconsDefaultFolderOpenSymbol = ''

"Custom symbols (more Font Awesome basically)

let s:file_node_extensions = {
            \ 'htm'      : '',
            \ 'html'     : '',
            \ 'slim'     : '',
            \ 'haml'     : '',
            \ 'ejs'      : '',
            \ 'css'      : '',
            \ 'less'     : '',
            \ 'md'       : '',
            \ 'mdx'      : '',
            \ 'markdown' : '',
            \ 'rmd'      : '',
            \ 'json'     : 'ﬥ',
            \ 'webmanifest' : 'ﬥ',
            \ 'js'       : '',
            \ 'mjs'      : '',
            \ 'mustache' : '',
            \ 'hbs'      : '',
            \ 'jpg'      : '',
            \ 'jpeg'     : '',
            \ 'bmp'      : '',
            \ 'png'      : '',
            \ 'webp'     : '',
            \ 'gif'      : '',
            \ 'ico'      : '',
            \ 'go'       : 'ﳑ',
            \ 'ts'       : 'ﯤ'
            \}

let s:file_node_exact_matches = {
            \ '.gitignore'                       : '',
            \ 'license'                          : ''
            \}
